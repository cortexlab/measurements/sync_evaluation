find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_SYNC_EVAL gnuradio-sync_eval)

FIND_PATH(
    GR_SYNC_EVAL_INCLUDE_DIRS
    NAMES gnuradio/sync_eval/api.h
    HINTS $ENV{SYNC_EVAL_DIR}/include
        ${PC_SYNC_EVAL_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_SYNC_EVAL_LIBRARIES
    NAMES gnuradio-sync_eval
    HINTS $ENV{SYNC_EVAL_DIR}/lib
        ${PC_SYNC_EVAL_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-sync_evalTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_SYNC_EVAL DEFAULT_MSG GR_SYNC_EVAL_LIBRARIES GR_SYNC_EVAL_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_SYNC_EVAL_LIBRARIES GR_SYNC_EVAL_INCLUDE_DIRS)
