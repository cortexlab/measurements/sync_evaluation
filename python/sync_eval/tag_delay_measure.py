#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
import pmt
import time
from gnuradio import gr, uhd

class tag_delay_measure(gr.sync_block):
    """
    docstring for block tag_delay_measure
    """
    def __init__(self, burst_len=500, record_file="record.csv"):
        gr.sync_block.__init__(self,
            name="tag_delay_measure",
            in_sig=[np.complex64],
            out_sig=None)
        
        self.burst_len = burst_len

        self.samp_rate = 1

        self.expected_tag_name = "Expected"
        self.corr_tag_name = "Corr_max"
        self.expected_offset = 0
        self.expected_tx = 0 # -1 marks when previous expected has been matched

        self.record_file = record_file

        with open(self.record_file, "wt") as f:
            print("#TX, delay (samps), delay (s)", file=f)

        self.log = gr.logger(self.alias())
        self.log.info("Init")




    def work(self, input_items, output_items):
        in0 = input_items[0]
        
        # Get tags from uhd
        tags = self.get_tags_in_window(0, 0, len(in0))
        for tag in tags:
            tag_key = pmt.to_python(tag.key)   
            if tag_key == "rx_rate":
                self.samp_rate = pmt.to_python(tag.value)
            if tag_key == self.expected_tag_name: # We expect this to arrive burst_len samples before the correlation
                if self.expected_tx > 0:
                    self.log.error("Previous expected was not matched")
                    with open(self.record_file, "at") as f:
                        print(f"{self.expected_tx}, NaN, NaN", file=f)
                if abs(tag.offset - self.expected_offset) < self.burst_len:
                    self.log.error("Expected reception before end of previous burst")
                self.expected_offset = tag.offset + self.burst_len
                self.expected_tx = pmt.to_python(tag.value)
            if tag_key == self.corr_tag_name:
                offset_diff = tag.offset - self.expected_offset
                time_diff = offset_diff / self.samp_rate
                if self.expected_tx < 0:
                    self.log.error("No previous expected to match to")

                with open(self.record_file, "at") as f:
                    print(f"{self.expected_tx}, {offset_diff}, {time_diff}", file=f)
                self.expected_tx = -1
                



        return len(input_items[0])
