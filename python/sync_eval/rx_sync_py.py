#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
import pmt
import time
from gnuradio import gr, uhd
import threading

class rx_sync_py(gr.sync_block):
    """
    Put "Expected" tag on sample where we expect to recieve the beginning of a signal
    """
    def __init__(self, node_number=0):
        gr.sync_block.__init__(self,
            name="rx_sync_py",
            in_sig=[np.complex64],
            out_sig=[np.complex64])
        
        self.node_number = int(node_number)

        self.stream_samp_rate = 1
        self.stream_ref_time = 0
        self.stream_ref_offset = 0

        self.message_port_register_in(pmt.intern("comm_in"))
        self.set_msg_handler(pmt.intern("comm_in"), self.handle_comms)
        self.message_port_register_out(pmt.to_pmt("comm_out"))

        self.to_tag_buffer = []

        self.log = gr.logger(self.alias())
        self.log.info("Init")

    def handle_comms(self, msg):
        if not pmt.is_pair(msg):
            self.log.error("Recieved message is not a pair, wrong format!")
            return
        
        msg_type = pmt.to_python(pmt.car(msg))
        msg_val = pmt.cdr(msg)

        if msg_type == "alive":
            return # Do nothing, we are a reciever
        if msg_type == "sync_order":
            return # Do nothing, we are a reciever
        if msg_type == "sync_info":
            return # Do nothing, we are a reciever
        if msg_type == "tx_order":
            return # Do nothing, we are a reciever
        if msg_type == "tx_info":
            # We insert info of transmitter and tx time into a list, ordering by time
            tx_info_py = [pmt.to_python(pmt.car(msg_val)), pmt.to_python(pmt.cdr(msg_val))] # Info contains first id of transmitter, then transmission time
            for i, buff_elem in enumerate(self.to_tag_buffer):
                if tx_info_py[1] < buff_elem[1]:  # Insert element just before the earliest one that's later than element
                    self.to_tag_buffer.insert(i, tx_info_py)
                    break
            else: # If list is empty or everything is earlier, put at the end
                self.to_tag_buffer.append(tx_info_py)
    
    def offset_to_time(self, offset):
        off_diff = offset - self.stream_ref_offset
        time_diff = off_diff / self.stream_samp_rate
        corresp_time = self.stream_ref_time + time_diff

        return corresp_time
    
    def time_to_offset(self, time):
        time_diff = time - self.stream_ref_time
        off_diff = time_diff * self.stream_samp_rate
        corresp_offset = off_diff + self.stream_ref_offset

        return int(corresp_offset)

    def mark_expected_rx_burst(self, tx_number, expected_time):
        self.add_item_tag(0, self.time_to_offset(expected_time), pmt.to_pmt("Expected"), pmt.to_pmt(tx_number), pmt.to_pmt("rx_sync"))

    def work(self, input_items, output_items):
        # Simply copy inputs
        in0 = input_items[0]
        out = output_items[0]
        out[:] = in0

        # Get tags from uhd
        tags = self.get_tags_in_window(0, 0, len(in0))
        for tag in tags:
            tag_key = pmt.to_python(tag.key)
            if tag_key == "rx_time":
                seconds, fracs = pmt.to_python(tag.value)
                self.stream_ref_time =  float(seconds + fracs)      
                self.stream_ref_offset = tag.offset          
            if tag_key == "rx_rate":
                self.stream_samp_rate = pmt.to_python(tag.value)

        end_win_time = self.offset_to_time(self.nitems_read(0) + len(in0))  # Time corresponding to last available sample.
        # Insert tags if we expect reception within the current window
        
        while len(self.to_tag_buffer) > 0: # We create a tag for all elements in buffer that correspond to the current sample window
            if end_win_time > self.to_tag_buffer[0][1]:
                self.mark_expected_rx_burst(self.to_tag_buffer[0][0], self.to_tag_buffer[0][1])

                self.to_tag_buffer.pop(0)
            else:
                break


        return len(output_items[0])
