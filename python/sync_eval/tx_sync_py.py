#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
import pmt
import time
from gnuradio import gr, uhd

class tx_sync_py(gr.sync_block):
    """
    docstring for block tx_sync_py
    """
    def __init__(self, burst_len=500, burst_tag_name="burst_len", node_number=0):
        gr.sync_block.__init__(self,
            name="tx_sync_py",
            in_sig=[np.complex64],
            out_sig=[np.complex64])
        
        self.node_number = int(node_number)

        self.burst_len = burst_len
        self.burst_tag_name = burst_tag_name
        self.left_to_transmit = 0


        self.message_port_register_in(pmt.intern("comm_in"))
        self.set_msg_handler(pmt.intern("comm_in"), self.handle_comms)

        self.message_port_register_out(pmt.to_pmt("comm_out"))

        self.log = gr.logger(self.alias())
        self.log.info("Init")


    def handle_comms(self, msg):
        if not pmt.is_pair(msg):
            self.log.error("Recieved message is not a pair, wrong format!")
            return
        
        msg_type = pmt.to_python(pmt.car(msg))
        msg_val = pmt.cdr(msg)

        if msg_type == "alive":
            return # Do nothing, we are a transmitter
        if msg_type == "sync_order":
            return # Do nothing, we are a transmitter
        if msg_type == "sync_info":
            return # Do nothing, we are a transmitter
        if msg_type == "tx_order":
            if pmt.to_python(pmt.car(msg_val)) == self.node_number:
                if self.left_to_transmit > 0:
                    self.log.error(f"We still have {self.left_to_transmit} samples to transmit from a previous burst")

                transmit_time = pmt.to_python(pmt.cdr(msg_val))
                tag_time_val = pmt.to_pmt((int(transmit_time) , (transmit_time % 1)))
                self.add_item_tag(0, self.nitems_written(0), pmt.to_pmt("tx_time"), tag_time_val, pmt.to_pmt("tx_sync"))

                self.add_item_tag(0, self.nitems_written(0), pmt.to_pmt(self.burst_tag_name), pmt.to_pmt(self.burst_len), pmt.to_pmt("tx_sync"))

                info_msg = pmt.cons(pmt.to_pmt("tx_info"), msg_val)
                self.message_port_pub(pmt.to_pmt("comm_out"), info_msg)

                self.left_to_transmit = self.burst_len
            else:
                pass
                # self.log.debug("Recieved a tx order but for another node. Ignoring")
        if msg_type == "tx_info":
            return # Do nothing, we are a transmitter
    
    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        transmit_this_run = min(self.left_to_transmit, len(in0))
        
        out[:transmit_this_run] = in0[:transmit_this_run]
        self.left_to_transmit -= transmit_this_run
        return transmit_this_run
