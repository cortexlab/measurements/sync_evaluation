/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(autocorr_peak_detect.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(9320d8ae9e22cbfc6a280595eaed58b6)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/sync_eval/autocorr_peak_detect.h>
// pydoc.h is automatically generated in the build directory
#include <autocorr_peak_detect_pydoc.h>

void bind_autocorr_peak_detect(py::module& m)
{

    using autocorr_peak_detect    = gr::sync_eval::autocorr_peak_detect;


    py::class_<autocorr_peak_detect, gr::sync_block, gr::block, gr::basic_block,
        std::shared_ptr<autocorr_peak_detect>>(m, "autocorr_peak_detect", D(autocorr_peak_detect))

        .def(py::init(&autocorr_peak_detect::make),
           D(autocorr_peak_detect,make)
        )
        



        ;




}








