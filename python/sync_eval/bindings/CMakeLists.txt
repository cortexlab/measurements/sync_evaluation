# Copyright 2020 Free Software Foundation, Inc.
#
# This file is part of GNU Radio
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

########################################################################
# Check if there is C++ code at all
########################################################################
if(NOT sync_eval_sources)
    MESSAGE(STATUS "No C++ sources... skipping python bindings")
    return()
endif(NOT sync_eval_sources)

########################################################################
# Check for pygccxml
########################################################################
GR_PYTHON_CHECK_MODULE_RAW(
    "pygccxml"
    "import pygccxml"
    PYGCCXML_FOUND
    )

include(GrPybind)

########################################################################
# Python Bindings
########################################################################

list(APPEND sync_eval_python_files
    autocorr_peak_detect_python.cc python_bindings.cc)

GR_PYBIND_MAKE_OOT(sync_eval
   ../../..
   gr::sync_eval
   "${sync_eval_python_files}")

# copy bindings extension for use in QA test module
add_custom_command(TARGET sync_eval_python POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:sync_eval_python>
    ${PROJECT_BINARY_DIR}/test_modules/gnuradio/sync_eval/
)

install(TARGETS sync_eval_python DESTINATION ${GR_PYTHON_DIR}/gnuradio/sync_eval COMPONENT pythonapi)
