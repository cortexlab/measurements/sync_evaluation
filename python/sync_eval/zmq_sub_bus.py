#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#



from gnuradio import gr
from gnuradio import zeromq

class zmq_sub_bus(gr.hier_block2):
    """
    Hier block to agglomerate messages coming from a group of nodes via ZMQ SUB
    """
    def __init__(self, port = 5600, timeout = 100, bind = False):
        gr.hier_block2.__init__(self,
            "ZMQ SUB Bus",
            gr.io_signature(0, 0, 0),  # Input signature
            gr.io_signature(0, 0, 0)) # Output signature
        
        self.message_port_register_hier_out("out")

        self.port = port
        self.timeout = timeout
        self.bind = bind

        self.hostname_base = "mnode"
        self.start_node_num = 1
        self.stop_node_num = 42

        self.node_list = [i for i in range(self.start_node_num, self.stop_node_num+1)]


        self.zmq_block_list = []
        for i in self.node_list:
            self.zmq_block_list.append(zeromq.sub_msg_source(f"tcp://{self.hostname_base}{i}:{self.port}", self.timeout, self.bind))
            self.msg_connect((self.zmq_block_list[-1], 'out'), (self, 'out'))
