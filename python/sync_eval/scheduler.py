#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
import pmt
import time
from gnuradio import gr, uhd
import threading

class scheduler(gr.sync_block):
    """
    Block to handle clock synchronisation and (for one instance), schedule the operations (sync and tx) for all the nodes
    """
    def __init__(self, node_number=0, is_master = False, is_rx = False, tx_nodes = [], rx_nodes = [], burst_period = 0.5):
        gr.sync_block.__init__(self,
            name="scheduler",
            in_sig=[],
            out_sig=[])
        
        self.message_port_register_in(pmt.intern("comm_in"))
        self.set_msg_handler(pmt.intern("comm_in"), self.handle_comms)
        self.message_port_register_out(pmt.to_pmt("comm_out"))

        self.node_number = int(node_number)
        self.usrp_blk = None
        self.is_master = is_master
        self.is_rx = is_rx

        self.participating_nodes = tx_nodes + rx_nodes
        self.tx_nodes = tx_nodes
        self.burst_period = burst_period
        self.alive_nodes = []
        self.synced_nodes = []

        self.clock_reset = False
        self.start_clock_value = uhd.time_spec(0, 0)

        self.log = gr.logger(self.alias())
        self.log.info("Init")

    def set_usrp_blk(self, blk):
        self.usrp_blk = blk
    
    def send_alive(self):
        msg = pmt.cons(pmt.to_pmt("alive"), pmt.to_pmt(self.node_number))
        self.message_port_pub(pmt.to_pmt("comm_out"), msg)
        self.log.info(f"Node {self.node_number} is alive and running")

    def handle_comms(self, msg):
        if not pmt.is_pair(msg):
            self.log.error("Recieved message is not a pair, wrong format!")
            return
        
        msg_type = pmt.to_python(pmt.car(msg))
        msg_val = pmt.cdr(msg)

        if msg_type == "alive":
            if self.is_master:
                alive_node = pmt.to_python(msg_val)
                if alive_node in self.participating_nodes:
                    if alive_node in self.alive_nodes:
                        self.log.error(f"Recieved duplicate alive message from node {alive_node}")
                    else:
                        self.log.info(f"Recieved alive message from {alive_node}")
                        self.alive_nodes.append(alive_node)
                        if len(self.alive_nodes) == len(self.participating_nodes):
                            self.log.info("Everyone is alive, waiting for proper second timing")
                            while True: 
                                time_now = self.usrp_blk.get_time_now().get_real_secs()
                                time_previous_pps = self.usrp_blk.get_time_last_pps().get_real_secs()
                                if time_now - time_previous_pps < 0.5: # We want to ensure we give enough time for every note to recive and reset clock before next PPS
                                    break
                                else:
                                    time.sleep(0.1)
                            self.log.info("Sending sync order to everyone")
                            info_msg = pmt.cons(pmt.to_pmt("sync_order"), pmt.to_pmt(self.node_number))
                            self.message_port_pub(pmt.to_pmt("comm_out"), info_msg)
                else:
                    self.log.error(f"Node {alive_node} sent alive message but is not in participating list")

            # Else do nothing
        if msg_type == "sync_order":
            self.do_sync()
        if msg_type == "sync_info":
            if self.is_master:
                synced_node = pmt.to_python(msg_val)
                if synced_node in self.participating_nodes:
                    if synced_node in self.synced_nodes:
                        self.log.error(f"Recieved duplicate sync_info message from node {synced_node}")
                    else:
                        self.log.info(f"Recieved sync_info message from {synced_node}")
                        self.synced_nodes.append(synced_node)
                        if len(self.synced_nodes) == len(self.participating_nodes):
                            self.log.info("Everyone is synced, beginning transmission")

                            self.socket_thread = threading.Thread(target=self.scheduler)
                            self.socket_thread.daemon = True
                            self.socket_thread.start()
                else:
                    self.log.error(f"Node {synced_node} sent sync_info message but is not in participating list")
            # Else do nothing
        if msg_type == "tx_order":
            pass # Do nothing, we are a scheduler
        if msg_type == "tx_info":
            pass # Do nothing, we are a scheduler

    def scheduler(self):
        i = 0
        while 1:
            current_time = self.usrp_blk.get_time_now().get_real_secs()
            order_msg = pmt.cons(pmt.to_pmt("tx_order"), pmt.cons(pmt.to_pmt(self.tx_nodes[i]), pmt.to_pmt(current_time+self.burst_period)))
            self.message_port_pub(pmt.to_pmt("comm_out"), order_msg)

            time.sleep(self.burst_period)
            i = (i + 1) % len(self.tx_nodes)

    
    def do_sync(self):
        reset_trig_time = self.usrp_blk.get_time_now().get_real_secs()
        self.usrp_blk.set_time_next_pps(self.start_clock_value)
        self.clock_reset = False        
        
        while not self.clock_reset:
            curr_usrp_time = self.usrp_blk.get_time_now().get_real_secs()
            if curr_usrp_time < reset_trig_time:
                self.log.info("Clock reset, start uhd source stream")
                self.clock_reset = True
                if self.is_rx:
                    self.start_uhd_stream(curr_usrp_time)
                break
            time.sleep(0.1)
        
        info_msg = pmt.cons(pmt.to_pmt("sync_info"), pmt.to_pmt(self.node_number))
        self.message_port_pub(pmt.to_pmt("comm_out"), info_msg)


    def start_uhd_stream(self, current_time):
        cmd = uhd.stream_cmd_t(uhd.stream_mode_t.STREAM_MODE_START_CONTINUOUS)
        cmd.stream_now = True
        self.usrp_blk.issue_stream_cmd(cmd)

    def stop_uhd_stream(self, current_time):
        cmd = uhd.stream_cmd_t(uhd.stream_mode_t.STREAM_MODE_STOP_CONTINUOUS)
        cmd.stream_now = True
        self.usrp_blk.issue_stream_cmd(cmd)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        # <+signal processing here+>
        out[:] = in0
        return len(output_items[0])
