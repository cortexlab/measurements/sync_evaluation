/* -*- c++ -*- */
/*
 * Copyright 2023 cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "autocorr_peak_detect_impl.h"

namespace gr {
  namespace sync_eval {

    using input_type = float;
    using output_type = float;
    autocorr_peak_detect::sptr
    autocorr_peak_detect::make(int preamble_len, float threshold)
    {
      return gnuradio::make_block_sptr<autocorr_peak_detect_impl>(
        preamble_len, threshold);
    }


    /*
     * The private constructor
     */
    autocorr_peak_detect_impl::autocorr_peak_detect_impl(int preamble_len, float threshold)
      : gr::sync_block("autocorr_peak_detect",
              gr::io_signature::make(1 /* min inputs */, 1 /* max inputs */, sizeof(input_type)),
              gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(output_type)))
    {}

    /*
     * Our virtual destructor.
     */
    autocorr_peak_detect_impl::~autocorr_peak_detect_impl()
    {
    }

    int
    autocorr_peak_detect_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      auto in = static_cast<const input_type*>(input_items[0]);
      auto out = static_cast<output_type*>(output_items[0]);

      #pragma message("Implement the signal processing in your block and remove this warning")
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace sync_eval */
} /* namespace gr */
