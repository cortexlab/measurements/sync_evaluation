/* -*- c++ -*- */
/*
 * Copyright 2023 cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_IMPL_H
#define INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_IMPL_H

#include <gnuradio/sync_eval/autocorr_peak_detect.h>

namespace gr {
  namespace sync_eval {

    class autocorr_peak_detect_impl : public autocorr_peak_detect
    {
     private:
      // Nothing to declare in this block.
      int d_preamble_len;
      float d_thresh;

     public:
      autocorr_peak_detect_impl(int preamble_len, float threshold);
      ~autocorr_peak_detect_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace sync_eval
} // namespace gr

#endif /* INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_IMPL_H */
