# this module will be imported in the into your flowgraph
import numpy as np

def zc_generator(length=17, root_id=0):
    """
    Generate a Zadoff-Chu sequence.

    Parameters
    ----------
    length : int
        Length of the sequence to generate
    root_id : int
        Root index

    Returns
    -------
    np.ndarray[np.complex64]
        Generated Zadoff-Chu 1d complex sequence
    """

    n_vec = np.arange(0, length)
    frac_vec = (n_vec * (n_vec + 1)) / length
    inner_vec = -1j * np.pi * root_id * frac_vec
    
    return np.exp(inner_vec)