#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Octoclock timing evaluation (TX)
# Author: Cyrille Morin
# GNU Radio version: 3.10.5.1

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import sync_eval
from gnuradio import uhd
import time
from gnuradio import zeromq
import octo_timing_tx_utils as utils  # embedded python module


def snipfcn_snippet_0(self):
    self.sched.send_alive()

def snipfcn_snippet_1(self):
    self.sched.set_usrp_blk(self.usrp_blk)

    if not self.init_sync:
        time.sleep(5)


def snippets_main_after_init(tb):
    snipfcn_snippet_1(tb)

def snippets_main_after_start(tb):
    snipfcn_snippet_0(tb)


class octo_timing_tx(gr.top_block):

    def __init__(self, burst_period=0.5, center_freq=680000000, init_sync=0, node_id=16, rx_nodes='0', samp_rate=200000, tx_gain=0, tx_nodes='0'):
        gr.top_block.__init__(self, "Octoclock timing evaluation (TX)", catch_exceptions=True)

        ##################################################
        # Parameters
        ##################################################
        self.burst_period = burst_period
        self.center_freq = center_freq
        self.init_sync = init_sync
        self.node_id = node_id
        self.rx_nodes = rx_nodes
        self.samp_rate = samp_rate
        self.tx_gain = tx_gain
        self.tx_nodes = tx_nodes

        ##################################################
        # Variables
        ##################################################
        self.preamble_len = preamble_len = 257
        self.length_tag = length_tag = "burst_length"

        ##################################################
        # Blocks
        ##################################################

        self.zeromq_pub_msg_sink_0 = zeromq.pub_msg_sink("tcp://*:5600", 100, True)
        self.usrp_blk = uhd.usrp_sink(
            ",".join(("", '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            length_tag,
        )
        self.usrp_blk.set_clock_source('external', 0)
        self.usrp_blk.set_time_source('external', 0)
        self.usrp_blk.set_samp_rate(samp_rate)
        _last_pps_time = self.usrp_blk.get_time_last_pps().get_real_secs()
        # Poll get_time_last_pps() every 50 ms until a change is seen
        while(self.usrp_blk.get_time_last_pps().get_real_secs() == _last_pps_time):
            time.sleep(0.05)
        # Set the time to PC time on next PPS
        self.usrp_blk.set_time_next_pps(uhd.time_spec(int(time.time()) + 1.0))
        # Sleep 1 second to ensure next PPS has come
        time.sleep(1)

        self.usrp_blk.set_center_freq(center_freq, 0)
        self.usrp_blk.set_antenna("TX/RX", 0)
        self.usrp_blk.set_bandwidth(samp_rate, 0)
        self.usrp_blk.set_gain(tx_gain, 0)
        self.sync_eval_zmq_sub_bus_0 = sync_eval.zmq_sub_bus(5600, 100, False)
        self.sync_eval_tx_sync_py_0 = sync_eval.tx_sync_py((preamble_len*2), length_tag, node_id)
        self.sched = sync_eval.scheduler(node_id, init_sync, False, [int(item) for item in tx_nodes.split(',')], [int(item) for item in rx_nodes.split(',')], 0.5)
        self.blocks_vector_source_x_1 = blocks.vector_source_c(utils.zc_generator(preamble_len,1), True, 1, [])


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.sched, 'comm_out'), (self.zeromq_pub_msg_sink_0, 'in'))
        self.msg_connect((self.sync_eval_tx_sync_py_0, 'comm_out'), (self.zeromq_pub_msg_sink_0, 'in'))
        self.msg_connect((self.sync_eval_zmq_sub_bus_0, 'out'), (self.sched, 'comm_in'))
        self.msg_connect((self.sync_eval_zmq_sub_bus_0, 'out'), (self.sync_eval_tx_sync_py_0, 'comm_in'))
        self.connect((self.blocks_vector_source_x_1, 0), (self.sync_eval_tx_sync_py_0, 0))
        self.connect((self.sync_eval_tx_sync_py_0, 0), (self.usrp_blk, 0))


    def get_burst_period(self):
        return self.burst_period

    def set_burst_period(self, burst_period):
        self.burst_period = burst_period

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.usrp_blk.set_center_freq(self.center_freq, 0)

    def get_init_sync(self):
        return self.init_sync

    def set_init_sync(self, init_sync):
        self.init_sync = init_sync

    def get_node_id(self):
        return self.node_id

    def set_node_id(self, node_id):
        self.node_id = node_id

    def get_rx_nodes(self):
        return self.rx_nodes

    def set_rx_nodes(self, rx_nodes):
        self.rx_nodes = rx_nodes

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.usrp_blk.set_samp_rate(self.samp_rate)
        self.usrp_blk.set_bandwidth(self.samp_rate, 0)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.usrp_blk.set_gain(self.tx_gain, 0)

    def get_tx_nodes(self):
        return self.tx_nodes

    def set_tx_nodes(self, tx_nodes):
        self.tx_nodes = tx_nodes

    def get_preamble_len(self):
        return self.preamble_len

    def set_preamble_len(self, preamble_len):
        self.preamble_len = preamble_len
        self.blocks_vector_source_x_1.set_data(utils.zc_generator(self.preamble_len,1), [])

    def get_length_tag(self):
        return self.length_tag

    def set_length_tag(self, length_tag):
        self.length_tag = length_tag



def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-p", "--burst-period", dest="burst_period", type=eng_float, default=eng_notation.num_to_str(float(0.5)),
        help="Set Burst period [default=%(default)r]")
    parser.add_argument(
        "-f", "--center-freq", dest="center_freq", type=eng_float, default=eng_notation.num_to_str(float(680000000)),
        help="Set Center Frequency [default=%(default)r]")
    parser.add_argument(
        "-i", "--init-sync", dest="init_sync", type=intx, default=0,
        help="Set Initialise sync [default=%(default)r]")
    parser.add_argument(
        "-n", "--node-id", dest="node_id", type=intx, default=16,
        help="Set Node Id [default=%(default)r]")
    parser.add_argument(
        "-r", "--rx-nodes", dest="rx_nodes", type=str, default='0',
        help="Set Rx nodes [default=%(default)r]")
    parser.add_argument(
        "-s", "--samp-rate", dest="samp_rate", type=intx, default=200000,
        help="Set Sample Rate [default=%(default)r]")
    parser.add_argument(
        "-g", "--tx-gain", dest="tx_gain", type=eng_float, default=eng_notation.num_to_str(float(0)),
        help="Set TX Gain [default=%(default)r]")
    parser.add_argument(
        "-t", "--tx-nodes", dest="tx_nodes", type=str, default='0',
        help="Set Tx nodes [default=%(default)r]")
    return parser


def main(top_block_cls=octo_timing_tx, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(burst_period=options.burst_period, center_freq=options.center_freq, init_sync=options.init_sync, node_id=options.node_id, rx_nodes=options.rx_nodes, samp_rate=options.samp_rate, tx_gain=options.tx_gain, tx_nodes=options.tx_nodes)
    snippets_main_after_init(tb)
    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    snippets_main_after_start(tb)
    tb.wait()


if __name__ == '__main__':
    main()
