/* -*- c++ -*- */
/*
 * Copyright 2023 cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_H
#define INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_H

#include <gnuradio/sync_eval/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace sync_eval {

    /*!
     * \brief <+description of block+>
     * \ingroup sync_eval
     *
     */
    class SYNC_EVAL_API autocorr_peak_detect : virtual public gr::sync_block
    {
     public:
      typedef std::shared_ptr<autocorr_peak_detect> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of sync_eval::autocorr_peak_detect.
       *
       * To avoid accidental use of raw pointers, sync_eval::autocorr_peak_detect's
       * constructor is in a private implementation
       * class. sync_eval::autocorr_peak_detect::make is the public interface for
       * creating new instances.
       */
      static sptr make(int preamble_len, float threshold);
    };

  } // namespace sync_eval
} // namespace gr

#endif /* INCLUDED_SYNC_EVAL_AUTOCORR_PEAK_DETECT_H */
